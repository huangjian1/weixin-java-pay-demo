package com.github.binarywang.demo.wx.pay.controller;

/**
 * @Author: 黄建
 * @Date: 2020/4/8 23:31
 */

import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URLEncoder;

/**
 * Created by SqMax on 2018/3/23.
 */
@Controller
@RequestMapping("/api/wechat")
@Slf4j
public class WeChatController {

  @Autowired
  private WxMpService wxMpService;

  @GetMapping("/authorize")
  public String authorize(@RequestParam("returnUrl") String returnUrl){
//        WxMpService wxMpService=new WxMpServiceImpl();
    //1. 配置
    //2.调用方法
    String url="http://myproject.nat300.top/api/wechat/userInfo";
    String redirectUrl=wxMpService.oauth2buildAuthorizationUrl(url,WxConsts.OAuth2Scope.SNSAPI_BASE, URLEncoder.encode(returnUrl));
    log.info("【微信网页授权】获取code,redirectUrl={}",redirectUrl);
    return "redirect:"+redirectUrl;//重定向到下面一个方法
  }

  @GetMapping("/userInfo")
  public String userInfo(@RequestParam("code") String code,
                         @RequestParam("state") String returnUrl){
    WxMpOAuth2AccessToken wxMpOAuth2AccessToken=new WxMpOAuth2AccessToken();
    try {
      wxMpOAuth2AccessToken=wxMpService.oauth2getAccessToken(code);
    }catch (WxErrorException e){
      log.error("【微信网页授权】,{}",e);
      System.out.println("error");
    }
    String openId=wxMpOAuth2AccessToken.getOpenId();
    log.info("【微信网页授权】获取openid,returnUrl={}",returnUrl);
    return "redirect:"+ returnUrl+"?openid="+openId;

  }
  //以上两个方法是SDK方式微信网页授权的过程，
  // 访问http://sqmax.natapp1.cc/sell/wechat/authorize?returnUrl=http://www.imooc.com，
  //最终将会跳转到这个链接：http://www.imooc.com?openid={openid}


}
