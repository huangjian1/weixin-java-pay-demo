package com.github.binarywang.demo.wx.pay.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: 黄建
 * @Date: 2020/4/8 23:39
 */
@Configuration
@ConfigurationProperties(prefix = "wx.mp")
@Data
public class WxMpConfig {

  private String token;

  private String appid;

  private String appSecret;

  private String aesKey;


}
